# romance-studies-last-decades

1. Extract data from romance languages
1. Clean data:
    1. year
2. Number of Data (and per year)
3. Compare to German and English
3. Number of languages (and per year)
3. Publishers (and per year)
3. Location of publishers (also per year)
3. Prices (also per year)
3. Length (also per year)
3. Keywords of title
3. Keywords from the subjects